Important notes for reviewer:
=============================

1. This technical test implements the "Martian Robots problem" in the form of a command line project. 
2. All the requirements of the problem have been implemented with clean and clear components with self explanatory project structure.
3. This command line project has been developed on latest Xcode 9.4.1 with Swift 4.1 without any warnings or errors. 
4. Open the project in Xcode 9.4.1 by double-clicking 'MartianRobots.xcodeproj' file. To run the project press 'Cmd+R'. 
5. After running the project check the result of sample run in XCode console window.
6. The solution is verified with Sample data throughout the developement process and project's "main.swift" file contains the sample data and sample run of 4 different robots which are introduced to the grid with their initial position and orientation and after running sequence of instructions their final position and orientation is acquired and verified.
7. If i had given more time, i would have taken TDD approach towards development of components. 
8. If i had given more time, i would have given a UI to this project for ease of use and for fun. 