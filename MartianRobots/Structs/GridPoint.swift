//
//  GridPoint.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

struct GridPoint: CustomStringConvertible {
    var x,y: Int
    
    init(x: Int, y: Int) throws {
        guard x <= 50, y <= 50 else {
            throw GridError.invalidGridPointMaxCoordinateValue
        }
        
        self.x = x
        self.y = y
    }
    
    init(coordinates: String) throws {
        let coordinates = coordinates.components(separatedBy: CharacterSet.whitespaces)
        guard coordinates.count == 2, let x = Int(coordinates[0]), let y = Int(coordinates[1]) else {
            throw GridError.invalidGridPoint
        }
        try self.init(x: x, y: y)
    }
    
    var description: String {
        return "\(x) \(y)"
    }
}

func == (lhs: GridPoint, rhs: GridPoint) -> Bool {
    return lhs.x == rhs.x && lhs.y == rhs.y
}

func <= (lhs: GridPoint, rhs: GridPoint) -> Bool {
    return lhs.x <= rhs.x && lhs.y <= rhs.y
}
