//
//  main.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

do {
    // Sample Run with sample Data
    let grid = try Grid(bounds: "5 3")

    let rb1 = try Robot(positionAndOrientation: "0 0 E")
    grid.robot = rb1
    let rb1InitialPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot1 initial position: " + rb1InitialPosition)
    try grid.moveRobot(with: "FLF")
    let rb1FinalPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot1 final position after instruction FLF should be [1 1 N]: " + rb1FinalPosition)
    
    print()
    
    let rb2 = try Robot(positionAndOrientation: "1 1 E")
    grid.robot = rb2
    let rb2InitialPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot2 initial position: " + rb2InitialPosition)
    try grid.moveRobot(with: "RFRFRFRF")
    let rb2FinalPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot2 final position after instruction RFRFRFRF should be [1 1 E]: " + rb2FinalPosition)
    
    print()
    
    let rb3 = try Robot(positionAndOrientation: "3 2 N")
    grid.robot = rb3
    let rb3InitialPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot3 initial position: " + rb3InitialPosition)
    try grid.moveRobot(with: "FRRFLLFFRRFLL")
    let rb3FinalPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot3 final position after instruction FRRFLLFFRRFLL should be [3 3 N LOST]: " + rb3FinalPosition)
    
    print()
    
    let rb4 = try Robot(positionAndOrientation: "0 3 W")
    grid.robot = rb4
    let rb4InitialPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot4 initial position: " + rb4InitialPosition)
    try grid.moveRobot(with: "LLFFFLFLFL")
    let rb4FinalPosition = try grid.robotFinalPositionAndOrientationOnGrid()
    print("* Robot4 final position after instruction LLFFFLFLFL should be [3 3 N LOST]: " + rb4FinalPosition)
    
    
} catch let error as GridError {
    print(error.localizedDescription)
}



