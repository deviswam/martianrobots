//
//  Robot.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

class Robot {
    private(set) var position: GridPoint
    private(set) var orientation: Orientation
    weak var delegate: RobotDelegate?
    var isLost = false

    init(position: GridPoint, orientation: Orientation) {
        self.position = position
        self.orientation = orientation
    }
    
    convenience init(positionAndOrientation: String) throws {
        let positionAndOrientationComponents = positionAndOrientation.components(separatedBy: CharacterSet.whitespaces)
        guard positionAndOrientationComponents.count == 3,
            let x = Int(positionAndOrientationComponents[0]),
            let y = Int(positionAndOrientationComponents[1]),
            let orientation = Orientation(rawValue: Character(positionAndOrientationComponents[2])) else {
                throw GridError.invalidRobotPositionOnGrid
        }
        self.init(position: try GridPoint(x: x, y: y), orientation: orientation)
    }
    
    func turnLeft() {
        guard !isLost else { return }
        
        switch orientation {
        case .east:
            orientation = .north
        case .north:
            orientation = .west
        case .west:
            orientation = .south
        case .south:
            orientation = .east
        }
    }
    
    func turnRight() {
        guard !isLost else { return }
        
        switch self.orientation {
        case .east:
            orientation = .south
        case .south:
            orientation = .west
        case .west:
            orientation = .north
        case .north:
            orientation = .east
        }
    }
    
    func moveForward() throws {
        guard !isLost else { return }
        
        let nextPosition: GridPoint!
        switch self.orientation {
        case .east:
            nextPosition = try GridPoint(x: position.x + 1, y: position.y)
        case .west:
            nextPosition = try GridPoint(x: position.x - 1, y: position.y)
        case .north:
            nextPosition = try GridPoint(x: position.x, y: position.y + 1)
        case .south:
            nextPosition = try GridPoint(x: position.x, y: position.y - 1)
        }
        
        guard let delegate = delegate, delegate.isSafeToMove(from: self.position, to: nextPosition) else { return }
        self.position = nextPosition
    }
}
