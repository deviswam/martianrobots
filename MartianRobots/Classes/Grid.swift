//
//  Grid.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

class Grid: NSObject, RobotDelegate  {
    private let bounds: GridPoint
    private var robotScents = [GridPoint]()
    var robot: Robot? {
        didSet {
            robot?.delegate = self
        }
    }
    
    init(bounds: GridPoint) {
        self.bounds = bounds
    }
    
    convenience init(bounds: String) throws {
        do {
            let bounds = try GridPoint(coordinates: bounds)
            self.init(bounds: bounds)
        } catch let error {
            throw error
        }
    }
    
    func moveRobot(with instructions:String) throws {
        guard instructions.count < 100 else { throw GridError.invalidInstructionMaxLengthValue }
        try instructions.forEach { instruction in
            guard let instruction = Instruction(rawValue: instruction) else {
                throw GridError.invalidInstruction
            }
            try processInstruction(instruction)
        }
    }
    
    private func processInstruction(_ instruction: Instruction) throws {
        guard let robot = robot else { throw GridError.noRobotOnGrid }
        switch instruction {
        case .left:
            robot.turnLeft()
        case .right:
            robot.turnRight()
        case .forward:
            try robot.moveForward()
        }
    }
    
    func robotFinalPositionAndOrientationOnGrid() throws -> String {
        guard let robot = robot else { throw GridError.noRobotOnGrid }
        let positionAndOrientationStr = "\(robot.position.description) \(robot.orientation.rawValue)"
        return robot.isLost ? positionAndOrientationStr + " LOST" : positionAndOrientationStr
    }
    
    
    // MARK: RobotDelegate Methods
    
    func isSafeToMove(from fromGridPoint: GridPoint, to toGridPoint: GridPoint) -> Bool {
        var isSafe = false
        // Safe, if the 'toGridPoint' is not out of Grid bounds and 'fromGridPoint' is not in scent. Unsafe otherwise.
        if toGridPoint <= self.bounds {
            let isScentPresent = robotScents.contains { gPoint -> Bool in
                return gPoint == fromGridPoint
            }
            
            if !isScentPresent {
                isSafe = true
            }
        } else {
            // When 'toGridPoint' is greater than bounds then add it into 'scents' array to prevent future robots from falling.
            // Also declare current robot as 'LOST'
            let isScentPresent = robotScents.contains { gPoint -> Bool in
                return gPoint == fromGridPoint
            }
            if !isScentPresent {
                robotScents.append(fromGridPoint)
            }
            robot?.isLost = true
        }
        return isSafe
    }
}
