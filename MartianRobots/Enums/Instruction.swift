//
//  Instruction.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum Instruction: Character {
    case left = "L"
    case right = "R"
    case forward = "F"
}
