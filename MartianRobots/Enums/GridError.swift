//
//  GridError.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

enum GridError: Error {
    case invalidInstruction
    case invalidInstructionMaxLengthValue
    case noRobotOnGrid
    case invalidGridPoint
    case invalidGridPointMaxCoordinateValue
    case invalidRobotPositionOnGrid
    
    var localizedDescription: String {
        switch self {
        case .invalidInstruction:
            return "The supplied instruction set for robot movement is invalid."
        case .invalidInstructionMaxLengthValue:
            return "Instruction string must be less than 100 characters in length."
        case .noRobotOnGrid:
            return "The grid doesn't have a robot."
        case .invalidGridPoint:
            return "The supplied grid point is invalid."
        case .invalidGridPointMaxCoordinateValue:
            return "The maximum coordinate value cannot be more than 50"
        case .invalidRobotPositionOnGrid:
            return "The supplied robot position on grid is invalid."
        }
    }
}
