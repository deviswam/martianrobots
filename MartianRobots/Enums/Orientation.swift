//
//  Orientation.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

enum Orientation: Character {
    case east = "E"
    case west = "W"
    case north = "N"
    case south = "S"
}
