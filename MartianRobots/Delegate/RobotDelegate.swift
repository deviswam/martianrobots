//
//  RobotDelegate.swift
//  MartianRobots
//
//  Created by Waheed Malik on 27/08/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

protocol RobotDelegate: NSObjectProtocol {
    func isSafeToMove(from fromGridPoint: GridPoint, to toGridPoint: GridPoint) -> Bool
}
